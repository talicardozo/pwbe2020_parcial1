package py.com.pwbe.laboratorio.modelo;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Mesas")
public class Mesa {

    @Id
    @Column(name = "id_Mesa")
    @Basic(optional = false)
    @GeneratedValue(generator = "mesaSec")
    @SequenceGenerator(name = "mesaSec",sequenceName = "Mesas_id_seq",allocationSize = 0)
    private Integer id_Mesa;
    @Column(name = "nombre", length = 50)
	@Basic(optional=false)
	private String nombre;
	@Column(name = "posicionx")
	@Basic(optional=false)
	private Integer posicionx;
	@Column(name = "posiciony")
	@Basic(optional=false)
	private Integer posiciony;
	@Column(name = "planta")
	@Basic(optional=false)
	private Integer planta;
	@Column(name = "capacidad")
	@Basic(optional=false)
	private Integer capacidad;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "id_Restaurante", referencedColumnName = "id_Restaurante")
    private Restaurante restaurante;
    
    @OneToMany(mappedBy = "mesa", cascade = CascadeType.ALL)
    private List<Reserva> listareservas;

    public Mesa() {

    }

    public void updateMesa(Mesa c) {
		this.nombre = c.nombre;
		this.posiciony = c.posiciony;
		this.planta = c.planta;
		this.capacidad = c.capacidad;
		this.posicionx = c.posicionx;
	}
    
    public Integer getIdMesa() {
		return this.id_Mesa;
	}
	
	public void setIdMesa(Integer idmesa) {
		this.id_Mesa = idmesa;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Integer getPosicionx() {
		return this.posicionx;
	}
	
	public void setPosicionx(Integer x) {
		this.posicionx = x;
	}
	
	public Integer getPosiciony() {
		return this.posiciony;
	}
	
	public void setPosiciony(Integer y) {
		this.posiciony = y;
	}
	
	public Integer getCapacidad() {
		return this.capacidad;
	}
	
	public void setCapacidad(Integer c) {
		this.capacidad = c;
	}
	
	public Integer getPlanta() {
		return this.planta;
	}
	
	public void setPlanta(Integer x) {
		this.planta = x;
	}
    
    public Restaurante getRestaurante() {
        return this.restaurante;
    }

    public void setRestaurante(Restaurante resta) {
        this.restaurante = resta;
    }
    
    public List<Reserva> getListaReservas() {
        return this.listareservas;
    }

    public void setListaReservas(Reserva reserv) {
        this.listareservas.add(reserv);
        if (reserv.getMesa() != this) {
            reserv.setMesa(this);
        }
    }
}
