package py.com.pwbe.laboratorio.modelo;

import javax.persistence.*;

import java.io.Serializable;

import java.util.List;

@Entity
@Table(name = "Clientes")
public class Cliente implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
    @Column(name = "id_Cliente")
    @Basic(optional = false)
    @GeneratedValue(generator = "clienteSec")
    @SequenceGenerator(name = "clienteSec",sequenceName = "Clientes_id_seq",allocationSize = 0)
    private Integer id;
    @Column(name = "nombre",length = 50)
    @Basic(optional = false)
    private String nombre;
    @Column(name = "apellido",length = 50)
    @Basic(optional = false)
    private String apellido;
    @Column(name = "cedula")
	@Basic(optional=false)
	private Integer cedula;

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
    private List<Reserva> listareservas;


    public Cliente(){

    }

    public Integer getIdCliente() {
        return id;
    }

    public void setIdCliente(Integer idcliente) {
        this.id = idcliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getCedula() {
		return this.cedula;
	}
	
	public void setCedula(Integer cedula) {
		this.cedula = cedula;
	}
	
    public List<Reserva> getListaReservas() {
        return this.listareservas;
    }

    public void setListaReservas(Reserva reserv) {
        this.listareservas.add(reserv);
        if (reserv.getCliente() != this) {
            reserv.setCliente(this);
        }
    }

    @Override
    public String toString() {
        return nombre+ " "+apellido;
    }
}
