package py.com.pwbe.laboratorio.modelo;

import javax.persistence.*;

import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Restaurantes")
public class Restaurante implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
    @Column(name = "id_Restaurante")
    @Basic(optional = false)
    @GeneratedValue(generator = "restauranteSec")
    @SequenceGenerator(name = "restauranteSec",sequenceName = "Restaurantes_id_seq",allocationSize = 0)
    private Integer id_Restaurante;
	@Column(name = "nombre", length = 50)
	@Basic(optional=false)
	private String nombre;
	@Column(name = "direccion", length = 50)
	@Basic(optional=false)
	private String direccion;

    @OneToMany(mappedBy = "restaurante", cascade = CascadeType.ALL)
    private List<Reserva> listareservas;
    @OneToMany(mappedBy = "restaurante", cascade = CascadeType.ALL)
    private List<Mesa> listamesas;


    public Restaurante(){

    }

    public void updateRestaurante(Restaurante c) {
		this.nombre = c.nombre;
		this.direccion = c.direccion;
	}
    
    public Integer getIdCliente() {
        return id_Restaurante;
    }

    public void setIdCliente(Integer idresta) {
        this.id_Restaurante = idresta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direc) {
        this.direccion = direc;
    }

    public List<Reserva> getListaReservas() {
        return this.listareservas;
    }

    public void setListaReservas(Reserva reserv) {
        this.listareservas.add(reserv);
        if (reserv.getRestaurante() != this) {
            reserv.setRestaurante(this);
        }
    }
    
    public List<Mesa> getListaMesas() {
        return this.listamesas;
    }

    public void setListaMesas(Mesa mes) {
        this.listamesas.add(mes);
    }

    @Override
    public String toString() {
        return nombre+ " "+direccion;
    }
}
