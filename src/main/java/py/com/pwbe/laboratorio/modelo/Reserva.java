package py.com.pwbe.laboratorio.modelo;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "Reservas")
public class Reserva {

    @Id
    @Column(name = "id_Reserva")
    @Basic(optional = false)
    @GeneratedValue(generator = "reservaSec")
    @SequenceGenerator(name = "reservaSec",sequenceName = "Reservas_id_seq",allocationSize = 0)
    private Integer id_Reserva;
    @Column(name = "cantidad", length = 50)
	@Basic(optional=false)
	private String cantidad;
    @Column(name = "fecha")
    @Basic(optional = false)
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "rangohora")
	@Basic(optional=false)
	private Integer rangohora;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "id_Cliente", referencedColumnName = "id_Cliente")
    private Cliente cliente;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "id_Restaurante", referencedColumnName = "id_Restaurante")
    private Restaurante restaurante;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "id_Mesa", referencedColumnName = "id_Mesa")
    private Mesa mesa;

    public Reserva() {

    }

    public Integer getIdReserva() {
        return id_Reserva;
    }

    public void setIdReserva(Integer idreserva) {
        this.id_Reserva = idreserva;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    public Integer getRangoHora() {
        return id_Reserva;
    }

    public void setRangoHora(Integer idrango) {
        this.rangohora = idrango;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente persona) {
        this.cliente = persona;
    }
    
    public Restaurante getRestaurante() {
        return restaurante;
    }

    public void setRestaurante(Restaurante resta) {
        this.restaurante = resta;
    }
    
    public Mesa getMesa() {
        return this.mesa;
    }

    public void setMesa(Mesa mes) {
        this.mesa = mes;
    }
}
