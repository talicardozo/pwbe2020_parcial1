package py.com.pwbe.laboratorio.ejb;

import py.com.pwbe.laboratorio.modelo.Reserva;
import py.com.pwbe.laboratorio.modelo.Restaurante;
import py.com.pwbe.laboratorio.modelo.Mesa;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
public class RestauranteDAO {
    @PersistenceContext(unitName = "laboratorioPU")
    EntityManager em;

    public void agregar(Restaurante entidad) {
        this.em.merge(entidad);
    }

    public void modificar(Integer id, Restaurante entidad) {
    	Restaurante c = this.em.find(Restaurante.class, id);
    	c.updateRestaurante(entidad);
        this.em.merge(c);
    }

    public void eliminar(Integer id) {
        this.em.remove(this.em.find(Restaurante.class,id));
    }
    
    public Restaurante getRestaurante(Integer id) {
    	return this.em.find(Restaurante.class, id);
    }
    
    public void setListaMesas(Integer id) {
    	Restaurante entidad = this.em.find(Restaurante.class,id);
    	Mesa reserv = this.em.find(Mesa.class,id);
        entidad.setListaMesas(reserv);
    }
    
    public List<Mesa> getListaMesas(Integer id) {
    	Restaurante entidad = this.em.find(Restaurante.class,id);
        return entidad.getListaMesas();
    }
    
    public void setListaReservas(Integer id) {
    	Restaurante entidad = this.em.find(Restaurante.class,id);
    	Reserva reserv = this.em.find(Reserva.class,id);
        entidad.setListaReservas(reserv);
    }
    
    public List<Reserva> getListaReservas(Integer id) {
    	Restaurante entidad = this.em.find(Restaurante.class,id);
        return entidad.getListaReservas();
    }
    
    public List<Mesa> getMesasDisponibles(Integer id, Date fecha, Integer[] horas){
    	Restaurante entidad = this.em.find(Restaurante.class, id);
    	List<Reserva> reservs = entidad.getListaReservas();
    	List<Integer> mesas = new ArrayList<Integer>();
    	List<Mesa> disponibles = entidad.getListaMesas();
    	if(reservs != null) {
    		for(Reserva r : reservs) {
    			boolean found = false;
    			for (Integer n : horas) {
    				if (n == r.getRangoHora()) {
    					found = true;
    					break;
    				}
    			}
    			if(r.getFecha() == fecha && found) {
    				mesas.add(r.getMesa().getIdMesa());
    			}
    		}    		
    	}
    	for(Integer i : mesas) {
    		for(Mesa m : disponibles) {
    			if(i.equals(m.getIdMesa())) {
    				disponibles.remove(m);
    			}
    		}
    	}
    	return disponibles;
    }

    @SuppressWarnings("unchecked")
    public List<Restaurante> lista() {
        Query q = em.createQuery("SELECT c FROM Restaurantes c");
        return (List<Restaurante>)q.getResultList();
    }
}
