package py.com.pwbe.laboratorio.ejb;

import javax.ejb.Stateless;
import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.pwbe.laboratorio.modelo.Mesa;
import py.com.pwbe.laboratorio.modelo.Reserva;


import java.util.List;

@Stateless
public class MesaDAO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@PersistenceContext(unitName = "laboratorioPU")
	 EntityManager em;
	 public void agregar(Mesa entidad) {
		 this.em.merge(entidad);
	 }
	    
	 public Mesa getMesa(Integer id) {
		 return this.em.find(Mesa.class, id);
	 }

	 public void modificar(Integer id, Mesa entidad) {
		 Mesa up = this.em.find(Mesa.class, id);
	   up.updateMesa(entidad);
	   this.em.merge(up);
	  }

	  public void eliminar(Integer id) {
	      this.em.remove(this.em.find(Mesa.class,id));
	  }
	  
	  public void setListaReservas(Integer id) {
		  Mesa entidad = this.em.find(Mesa.class,id);
		  Reserva reserv = this.em.find(Reserva.class,id);
		  entidad.setListaReservas(reserv);
	  }
    
	  public List<Reserva> getListaReservas(Integer id) {
		  Mesa entidad = this.em.find(Mesa.class,id);
		  return entidad.getListaReservas();
	  }

	   @SuppressWarnings("unchecked")
	   public List<Mesa> lista() {
	       Query q = em.createQuery("select m from Mesas m");
	       return (List<Mesa>)q.getResultList();
	   }
}
