package py.com.pwbe.laboratorio.ejb;

import py.com.pwbe.laboratorio.modelo.Cliente;
import py.com.pwbe.laboratorio.modelo.Reserva;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class ClienteDAO {
    @PersistenceContext(unitName = "laboratorioPU")
    EntityManager em;

    public void agregar(Cliente entidad) {
        this.em.merge(entidad);
    }

    public void modificar(Cliente entidad) {
        this.em.merge(entidad);
    }

    public void eliminar(Integer id) {
        this.em.remove(this.em.find(Cliente.class,id));
    }

    public Cliente getCliente(Integer idcliente) {
        return this.em.find(Cliente.class, idcliente);
    }
    
    public Cliente getClienteCedula(Integer cedula) {
        return this.em.find(Cliente.class, cedula);
    }
    
    public List<Reserva> getListaReservas(Integer id) {
    	Cliente entidad = this.em.find(Cliente.class,id);
        return entidad.getListaReservas();
    }

    @SuppressWarnings("unchecked")
    public List<Cliente> lista() {
        Query q = em.createQuery("SELECT p FROM Clientes p");
        return (List<Cliente>) q.getResultList();
    }
}
