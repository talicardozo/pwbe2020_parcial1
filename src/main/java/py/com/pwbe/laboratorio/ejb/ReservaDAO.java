package py.com.pwbe.laboratorio.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.pwbe.laboratorio.modelo.*;


import java.util.ArrayList;
import java.util.List;

@Stateless
public class ReservaDAO {

	@PersistenceContext(unitName = "laboratorioPU")
    EntityManager em;

    public void agregar(Integer idCliente, Integer idRestaurante, Integer idMesa, Reserva entidad) {
    	Cliente c = em.find(Cliente.class, idCliente); 
    	c.setListaReservas(entidad);
    	entidad.setCliente(c);
    	Restaurante r = em.find(Restaurante.class, idRestaurante);
    	r.setListaReservas(entidad);
    	entidad.setRestaurante(r);
    	Mesa m = em.find(Mesa.class, idRestaurante);
    	m.setListaReservas(entidad);
    	entidad.setMesa(m);
    	em.merge(c);
    	em.merge(r);
    	em.merge(m);
    	em.merge(entidad);
    }
    
    public Reserva getReserva(Integer id) {
    	return this.em.find(Reserva.class, id);
    }
    
    @SuppressWarnings("unchecked")
	public List<Reserva> lista() {
	    Query q = em.createQuery("select m from Reservas m");
	    return (List<Reserva>)q.getResultList();
	}
    
    @SuppressWarnings("unused")
	public List<Reserva> getReserva(Integer min, Integer max) {
    	
    	return new ArrayList<Reserva>();
    }

    public void eliminar(Integer id) {
        this.em.remove(this.em.find(Reserva.class,id));
    }
}
