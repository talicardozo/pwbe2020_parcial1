package py.com.pwbe.laboratorio.rest;


import py.com.pwbe.laboratorio.ejb.MesaDAO;
import py.com.pwbe.laboratorio.modelo.Mesa;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("mesas")
@Consumes("application/json")
@Produces("application/json")
public class MesaRest {

    @Inject
    MesaDAO mesaDAO;

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response agregar(Mesa persona) {
        System.out.println(persona.getNombre());
        mesaDAO.agregar(persona);
        return Response.ok().build();
    }

    @GET
    @Path("/")
    public Response lista() {
        return Response.ok(mesaDAO.lista()).build();
    }

    @GET
    @Path("/{cliente:[0-9][0-9]*}")
    public Response getPersona(@PathParam("cliente") Integer idPersona) {
        return Response.ok(mesaDAO.getMesa(idPersona)).build();
    }
}
