package py.com.pwbe.laboratorio.rest;


import py.com.pwbe.laboratorio.ejb.ReservaDAO;
import py.com.pwbe.laboratorio.modelo.Reserva;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("reservas")
@Consumes("application/json")
@Produces("application/json")
public class ReservaRest {

    @Inject
    ReservaDAO reservaDAO;

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response agregar(@QueryParam("idCliente") Integer idCliente, @QueryParam("idRestaurante") Integer idRestaurante, @QueryParam("idMesa") Integer idMesa, Reserva reserv) {
        System.out.println(reserv.getRestaurante());
        reservaDAO.agregar(idCliente, idRestaurante, idMesa, reserv);
        return Response.ok().build();
    }

    @GET
    @Path("/")
    public Response lista() {
        return Response.ok(reservaDAO.lista()).build();
    }

    @GET
    @Path("/{cliente:[0-9][0-9]*}")
    public Response getPersona(@PathParam("reserva") Integer idPersona) {
        return Response.ok(reservaDAO.getReserva(idPersona)).build();
    }
}
