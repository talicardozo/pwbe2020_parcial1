package py.com.pwbe.laboratorio.rest;


import py.com.pwbe.laboratorio.ejb.ClienteDAO;
import py.com.pwbe.laboratorio.modelo.Cliente;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("clientes")
@Consumes("application/json")
@Produces("application/json")
public class ClienteRest {

    @Inject
    ClienteDAO clienteDAO;

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response agregar(Cliente persona) {
        System.out.println(persona.getNombre() + persona.getApellido() + persona.getIdCliente());
        clienteDAO.agregar(persona);
        return Response.ok().build();
    }

    @GET
    @Path("/")
    public Response lista() {
        return Response.ok(clienteDAO.lista()).build();
    }

    @GET
    @Path("/{cliente:[0-9][0-9]*}")
    public Response getPersona(@PathParam("cliente") Integer idPersona) {
        return Response.ok(clienteDAO.getCliente(idPersona)).build();
    }
}
