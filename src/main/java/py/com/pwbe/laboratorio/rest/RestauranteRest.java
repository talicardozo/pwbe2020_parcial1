package py.com.pwbe.laboratorio.rest;


import py.com.pwbe.laboratorio.ejb.RestauranteDAO;
import py.com.pwbe.laboratorio.modelo.Restaurante;

import java.util.Date;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("restaurantes")
@Consumes("application/json")
@Produces("application/json")
public class RestauranteRest {

    @Inject
    RestauranteDAO personaDAO;

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response agregar(Restaurante persona) {
        System.out.println(persona.getNombre() + persona.getDireccion());
        personaDAO.agregar(persona);
        return Response.ok().build();
    }

    @GET
    @Path("/")
    public Response lista() {
        return Response.ok(personaDAO.lista()).build();
    }
    
    @GET
    @Path("/disponibles")
    public Response disponibles() {
        return Response.ok(personaDAO.lista()).build();
    }
    public Response disponibles(@QueryParam("idRestaurante") Integer idRestaurante, @QueryParam("fecha") Date fecha, @QueryParam("horas") Integer[] horas) {
        
        return Response.ok(personaDAO.getMesasDisponibles(idRestaurante, fecha, horas)).build();
    }

    @GET
    @Path("/{restaurante:[0-9][0-9]*}")
    public Response getRestaurante(@PathParam("restaurante") Integer idRest) {
        return Response.ok(personaDAO.getRestaurante(idRest)).build();
    }
}
